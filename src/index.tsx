import ForgeUI, { render, Fragment, Text, Cell, useAction, Head, Row, Table } from "@atlassian/forge-ui";

async function getJiraDara(): Promise<any> {
    const jql = `project = EC AND status = "In Progress"`;
    const response = await api.asUser().requestJira(`/rest/api/3/search?jql=${jql}`);
    const json = await response.json();

    return json;
}

const App = () => {
    const [jiraData] = useAction(value=>value, ()=>getJiraDara());

  return (
    <Fragment>
        <Table>
            <Head>
                <Cell>
                    <Text content="Key"/>
                </Cell>
                <Cell>
                    <Text content="Summary"/>
                </Cell>
            </Head>
            {jiraData.issues.map(issue=>(
                <Row>
                    <Cell>
                        <Text content={issue.key}/>
                    </Cell>
                    <Cell>
                        <Text content={issue.fields.summary}/>
                    </Cell>
                </Row>
            ))}
        </Table>
    </Fragment>
  );
};

export const run = render(<App />);
