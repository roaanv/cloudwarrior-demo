# Forge Hello World

For documentation and tutorials, see: http://go/forge-docs
Feel free to reach out in the [#help-xen](https://atlassian.slack.com/messages/CK08DFL84) room too!

## Quick start

`npm install` to install dependencies

Edit the "hello world" app example in `index.tsx`

`npm run deploy` to build your app and deploy it!

To have your app visible in a site, run `forge install`. You only need to do this when you're installing your app on a site for the first time.

You can install the latest `forge` cli by running `npm install -g @atlassian/forge-cli`.